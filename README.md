# ModNeuro
A series of notebooks that serves as an introduction to computational/theoretical neuroscience presented as a website for the reader. 

[Website Here](https://dabane-ghassan.github.io/ModNeuro/)

![cover photo](images/skyline.png)
